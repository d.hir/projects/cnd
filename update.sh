if [[ $# -lt 1 ]]; then
    echo "ERROR: Missing argument VERSION"
    exit 1
fi
version=$1

git status | grep -q -E "(modified|Changes to be committed)"
if [[ $? -eq 0 ]]; then
    echo "ERROR: Must be executed in a clean git state"
    exit 1
fi

git tag | grep -q -E "^v$version$"
if [[ $? -eq 0 ]]; then
    echo "ERROR: Version tag v$version already exists"
    exit 1
fi

! file CMakeLists.txt | grep -q CRLF
usecrlf=$?


set -euo pipefail


echo "Updating version in CMakeLists.txt..."
sed -i -E "s/^(project\(cnd .* VERSION )..?\...?\...?(\))/\1$version\2/" CMakeLists.txt

if [[ $usecrlf -eq 1 ]]; then
    unix2dos -b CMakeLists.txt
fi

git add CMakeLists.txt

git commit -m "[VERSIONING] Bump version to $version"

git tag v$version
