set(CND_EXTERN ${CMAKE_CURRENT_LIST_DIR}/../extern)

set(IMGUI_ROOT ${CND_EXTERN}/imgui)
add_library(imgui STATIC)
target_sources(imgui
    PUBLIC
        ${IMGUI_ROOT}/imgui.h
    PRIVATE
        ${IMGUI_ROOT}/imgui.cpp
        ${IMGUI_ROOT}/imgui_demo.cpp
        ${IMGUI_ROOT}/imgui_draw.cpp
        ${IMGUI_ROOT}/imgui_internal.h
        ${IMGUI_ROOT}/imgui_tables.cpp
        ${IMGUI_ROOT}/imgui_widgets.cpp
)
target_include_directories(imgui PUBLIC ${IMGUI_ROOT} ${IMGUI_ROOT}/backends)


string(TOLOWER "${CND_GUI_RENDERER}" CND_GUI_RENDERER_LOWER)
if(CND_GUI_RENDERER_LOWER STREQUAL "opengl" OR CND_GUI_RENDERER_LOWER STREQUAL "opengl3")
    message(STATUS "Using GUI renderer OpenGL")

    find_package(OpenGL REQUIRED)

    target_link_libraries(imgui PRIVATE OpenGL::GL)

    target_sources(imgui
        PUBLIC
            ${IMGUI_ROOT}/backends/imgui_impl_opengl3.h
        PRIVATE
            ${IMGUI_ROOT}/backends/imgui_impl_opengl3_loader.h
            ${IMGUI_ROOT}/backends/imgui_impl_opengl3.cpp
    )
else()
    message(FATAL_ERROR "Requested GUI renderer \"" ${CND_GUI_RENDERER_LOWER} "\" is not supported")
endif()


string(TOLOWER "${CND_GUI_PLATFORM}" CND_GUI_PLATFORM_LOWER)
if(CND_GUI_PLATFORM_LOWER STREQUAL "glfw")
    message(STATUS "Using GUI platform GLFW")

    set( GLFW_BUILD_DOCS OFF CACHE BOOL  "GLFW lib only" )
    set( GLFW_INSTALL OFF CACHE BOOL  "GLFW lib only" )
    add_subdirectory(${CND_EXTERN}/glfw)
    
    target_link_libraries(imgui PUBLIC glfw)

    target_sources(imgui
        PUBLIC
            ${IMGUI_ROOT}/backends/imgui_impl_glfw.h
        PRIVATE
            ${IMGUI_ROOT}/backends/imgui_impl_glfw.cpp
    )
else()
    message(FATAL_ERROR "Requested GUI platform \"" ${CND_GUI_PLATFORM_LOWER} "\" is not supported")
endif()
