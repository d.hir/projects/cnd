#include "MainWindow.hpp"

namespace dhir::cnd::gui {


MainWindow::MainWindow() {
    _about = create_child<about::About>();
    _character = create_child<character::CharacterHandler>();
}

void MainWindow::_create_main_menu_bar() {
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("Character")) {
            ImGui::MenuItem("New", nullptr, _character->new_active());
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Help")) {
            ImGui::MenuItem("About", nullptr, &_about->active);
            ImGui::EndMenu();
        }
    }
    ImGui::EndMainMenuBar();
}

void MainWindow::content() {
    _create_main_menu_bar();
    ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());
}


}  // namespace dhir::cnd::gui
