#include "character/editor/CharacterEditor.hpp"

#include <format>

namespace dhir::cnd::gui::character::editor {


CharacterEditor::CharacterEditor(const id& char_id, const std::optional<std::string>& name) :
    Window("Character Editor"),
    character(char_id, name) {
    window_flags |= ImGuiWindowFlags_MenuBar;
}

void CharacterEditor::_create_menu_bar() {
    if (ImGui::BeginMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            bool closed = false;
            ImGui::MenuItem("Close", nullptr, &closed);
            active = !closed;
            ImGui::EndMenu();
        }
    }
    ImGui::EndMenuBar();
}

void CharacterEditor::content() {
    title = std::format("{} - Character Editor", character.name());
    _create_menu_bar();
}


}  // namespace dhir::cnd::gui::character::editor
