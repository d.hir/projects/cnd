#pragma once
#include "cnd/character/Character.hpp"
#include "cnd/util/id.hpp"
#include "util/Window.hpp"

#include <imgui.h>
#include <optional>
#include <string>

namespace dhir::cnd::gui::character::editor {


class CharacterEditor : public Window {
public:
    CharacterEditor(const id& identifier, const std::optional<std::string>& name = std::nullopt);

protected:
    void content() override;

private:
    cnd::Character character;
    void _create_menu_bar();
};


}  // namespace dhir::cnd::gui::character::editor
