#pragma once

#include "character/CharacterNew.hpp"
#include "character/editor/CharacterEditor.hpp"
#include "cnd/util/id.hpp"
#include "util/Window.hpp"

#include <map>
#include <memory>
#include <optional>
#include <string>

namespace dhir::cnd::gui::character {


class CharacterHandler : public Window {
public:
    CharacterHandler() noexcept;
    std::shared_ptr<editor::CharacterEditor> open(const id& char_id, const std::optional<std::string>& name);

    inline bool exists(const id& char_id) const;

    bool * new_active();

private:
    std::map<id, std::shared_ptr<editor::CharacterEditor>> _characters;
    std::shared_ptr<CharacterNew> _character_new;
};


}  // namespace dhir::cnd::gui::character
