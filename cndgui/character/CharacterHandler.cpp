#include "character/CharacterHandler.hpp"

#include <format>

namespace dhir::cnd::gui::character {


CharacterHandler::CharacterHandler() noexcept {
    active = true;
    _character_new = create_child<CharacterNew>(*this);
}

inline bool CharacterHandler::exists(const id& char_id) const {
    return _characters.contains(char_id);
}

std::shared_ptr<editor::CharacterEditor> CharacterHandler::open(const id& char_id, const std::optional<std::string>& name) {
    if (!exists(char_id)) {
        _characters[char_id] = create_child<editor::CharacterEditor>(char_id, name);
    }
    if (!_characters[char_id]) {
        throw std::runtime_error(std::format("Character with id {} is invalid", *char_id.data()));
    }
    _characters[char_id]->active = true;
    return _characters[char_id];
}

bool * CharacterHandler::new_active() {
    if (!_character_new) {
        throw std::runtime_error("New Character window invalid");
    }
    return &_character_new->active;
}


}  // namespace dhir::cnd::gui::character
