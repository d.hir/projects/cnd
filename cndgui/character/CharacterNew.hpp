#pragma once

#include "cnd/util/id.hpp"
#include "util/Window.hpp"

#include <optional>
#include <string>

namespace dhir::cnd::gui::character {


class CharacterHandler;

class CharacterNew : public Window {
public:
    explicit CharacterNew(CharacterHandler& handler);

protected:
    void content() override;

private:
    void reset();

    static const size_t BUFFER_SIZE = 256;

    CharacterHandler& handler;
    char name[BUFFER_SIZE] {'\0'};
    id identifier;
};


}  // namespace dhir::cnd::gui::character
