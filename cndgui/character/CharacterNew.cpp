#include "character/CharacterNew.hpp"

#include "character/CharacterHandler.hpp"
#include "util/ImGuiExtensions.hpp"

#include <imgui.h>
#include <iostream>

namespace dhir::cnd::gui::character {


CharacterNew::CharacterNew(CharacterHandler& handler) : Window("New Character"), handler(handler), identifier(id::random()) {
}

void CharacterNew::reset() {
    active = false;
    identifier.randomize();
    name[0] = '\0';
}

static void _set_min_size_second_column(float size) {
    ImGui::TableNextColumn();
    ImGui::TableNextColumn();
    ImGui::Dummy(ImVec2(500.0f, 0.0f));
}

void CharacterNew::content() {
    const ImGuiTableFlags table_flags = ImGuiTableFlags_SizingFixedFit;
    if (ImGui::BeginTable("about_table", 2, table_flags)) {
        _set_min_size_second_column(500.0f);

        ImGui::TableNextColumn();
        ImGui::Text("Name:");
        ImGui::TableNextColumn();
        ImGui::InputText("##name", &name[0], BUFFER_SIZE);

        ImGui::TableNextColumn();
        ImGui::Text("ID:");
        ImGui::TableNextColumn();
        int flags = 0;
        if constexpr (std::is_integral_v<id::value_type>) {
            flags |= ImGuiInputTextFlags_CharsHexadecimal;
        }
        ImGui::InputScalar("##id", extensions::ImGuiDataType<id::value_type>::value, identifier.data());

        ImGui::EndTable();
    }
    if (ImGui::Button("Abort")) {
        reset();
    }
    ImGui::SameLine();
    if (ImGui::Button("Create")) {
        handler.open(identifier, std::string(name));
        reset();
    }
}


}  // namespace dhir::cnd::gui::character
