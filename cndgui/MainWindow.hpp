#pragma once

#include "about/About.hpp"
#include "character/CharacterHandler.hpp"
#include "util/Window.hpp"

#include <imgui.h>

namespace dhir::cnd::gui {


class MainWindow : public Window {
public:
    MainWindow();

protected:
    void content() override;

private:
    std::shared_ptr<about::About> _about;
    std::shared_ptr<character::CharacterHandler> _character;

    void _create_main_menu_bar();
};


}  // namespace dhir::cnd::gui
