#pragma once

#define IMGUI_DEFINE_MATH_OPERATORS
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <memory>

// This is only defined in OpenGL 1.2+. So we manually define it if it isn't there
#ifndef GL_CLAMP_TO_EDGE
#define GL_CLAMP_TO_EDGE 0x812F
#endif

namespace dhir::cnd::gui {


class Texture {
public:
    const unsigned char * const data;
    const size_t width;
    const size_t height;
    const size_t len;

    Texture(const unsigned char * data, size_t width, size_t height, size_t len);
    Texture(const Texture& other) = delete;
    Texture& operator=(const Texture& other) = delete;
    Texture(Texture&& other) = default;
    Texture& operator=(Texture&& other) = default;
    ~Texture() = default;

    const ImVec2 size() const;
    void draw(float scale = 1.0f, bool center = false);

private:
    void ensure_gl_texture();

    std::unique_ptr<GLuint> texture {nullptr};
};


}  // namespace dhir::cnd::gui
