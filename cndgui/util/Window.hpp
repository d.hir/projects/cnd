#pragma once

#include <imgui.h>
#include <memory>
#include <string>
#include <vector>

namespace dhir::cnd::gui {


class Window;

template<typename W>
concept SpecificWindow = std::is_base_of_v<Window, W>;

class Window {
public:
    using window_pointer = std::shared_ptr<Window>;

    bool active = false;

    Window() noexcept;
    explicit Window(const std::string& title);
    virtual ~Window() = default;

    void draw();

protected:
    std::vector<window_pointer> children;
    std::string title = "";
    ImGuiWindowFlags window_flags {0};

    template<SpecificWindow W, typename... Args>
    std::shared_ptr<W> create_child(Args&&... args);

    virtual void content();

private:
    const bool is_meta_window;

    void window_content();
};

template<SpecificWindow W, typename... Args>
std::shared_ptr<W> Window::create_child(Args&&... args) {
    std::shared_ptr<W> window = std::make_shared<W>(std::forward<Args>(args)...);
    children.push_back(window);
    return window;
}


}  // namespace dhir::cnd::gui
