#include "util/Window.hpp"

#include <imgui.h>
#include <iostream>
#include <ranges>

namespace dhir::cnd::gui {


Window::Window() noexcept : is_meta_window {true} {
}

Window::Window(const std::string& title) : title(title), is_meta_window(false) {
}

void Window::draw() {
    window_content();
    std::vector<window_pointer> children_copy = children;
    for (auto& child : children_copy) {
        if (!child) {
            throw std::runtime_error("Invalid Child Window encountered");
        }
        if (child->active) {
            child->draw();
        }
    }
};

void Window::window_content() {
    if (is_meta_window) {
        content();
    } else {
        if (ImGui::Begin(title.c_str(), &active, window_flags)) {
            content();
        }
        ImGui::End();
    }
}

void Window::content() {};


}  // namespace dhir::cnd::gui
