#pragma once


#include <imgui.h>
#include <cstdint>


namespace dhir::cnd::gui::extensions {


template<typename T>
struct ImGuiDataType;

template<> struct ImGuiDataType<std::int8_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_S8; };
template<> struct ImGuiDataType<std::uint8_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_U8; };
template<> struct ImGuiDataType<std::int16_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_S16; };
template<> struct ImGuiDataType<std::uint16_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_U16; };
template<> struct ImGuiDataType<std::int32_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_S32; };
template<> struct ImGuiDataType<std::uint32_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_U32; };
template<> struct ImGuiDataType<std::int64_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_S64; };
template<> struct ImGuiDataType<std::uint64_t> { static constexpr ImGuiDataType_ value = ImGuiDataType_U64; };
template<> struct ImGuiDataType<float> { static constexpr ImGuiDataType_ value = ImGuiDataType_Float; };
template<> struct ImGuiDataType<double> { static constexpr ImGuiDataType_ value = ImGuiDataType_Double; };


}
