#pragma once

#include <imgui.h>


namespace dhir::cnd::gui::util {


class Centerer {
public:
    Centerer(ImVec2 windowSize) : windowSize_(windowSize) {
    }

    template<typename Func>
    bool operator()(Func control) const;

private:
    const ImVec2 windowSize_;
};


template<typename Func>
bool Centerer::operator()(Func imgui_call) const {
    const ImVec2 originalPos = ImGui::GetCursorPos();

    // Draw offscreen to calculate size
    ImGui::SetCursorPos(ImVec2(-10000.0f, -10000.0f));
    imgui_call();
    const ImVec2 controlSize = ImGui::GetItemRectSize();

    // Draw at centered position
    ImGui::SetCursorPos(ImVec2((windowSize_.x - controlSize.x) * 0.5f, originalPos.y));
    imgui_call();

    return ImGui::IsItemClicked();
}


#define CENTER(imgui_call) dhir::cnd::gui::util::Centerer {ImGui::GetContentRegionAvail()}([&]() { imgui_call; })


}  // namespace dhir::cnd::gui::util
