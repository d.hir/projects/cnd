#include "util/Texture.hpp"

#include "util/Center.hpp"

#include <iostream>
#include <stb_image.h>
#include <stdexcept>

namespace dhir::cnd::gui {


Texture::Texture(const unsigned char * data, size_t width, size_t height, size_t len) : data(data), width(width), height(height), len(len) {
}

void Texture::ensure_gl_texture() {
    if (texture) {
        return;
    }
    if (data == nullptr) {
        throw std::runtime_error("Cannot create OpenGL texture without image data");
    }
    int w = static_cast<int>(width);
    int h = static_cast<int>(height);
    int t = 0;
    unsigned char * imageData = stbi_load_from_memory(data, w * h * 4, &w, &h, &t, 0);
    if (imageData == nullptr) {
        std::cout << stbi_failure_reason() << std::endl;
        throw std::runtime_error(stbi_failure_reason());
    }

    texture = std::make_unique<GLuint>();
    glGenTextures(1, texture.get());
    glBindTexture(GL_TEXTURE_2D, *texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);  // This is required on WebGL for non power-of-two textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);  // Same

#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
    stbi_image_free(imageData);
}

const ImVec2 Texture::size() const {
    return ImVec2(static_cast<float>(width), static_cast<float>(height));
}

void Texture::draw(float scale, bool center) {
    ensure_gl_texture();
    ImTextureID img = (ImTextureID) static_cast<intptr_t>(*texture);
    const ImVec2 sz = size() * scale;
    if (center) {
        CENTER(ImGui::Image(img, sz));
    } else {
        ImGui::Image(img, sz);
    }
}



}  // namespace dhir::cnd::gui
