#include "about/About.hpp"

#include "resources/logo.hpp"
#include "util/Center.hpp"

#include <GLFW/glfw3.h>
#include <cnd/version.hpp>
#include <format>
#include <imgui.h>

namespace dhir::cnd::gui::about {


About::About() noexcept : Window("About") {
}

static void _row(const char * lhs, const char * rhs, const char * padding = nullptr) {
    ImGui::TableNextColumn();
    ImGui::Text(lhs);
    ImGui::TableNextColumn();
    if (padding != nullptr) {
        ImGui::Text(padding);
    }
    ImGui::TableNextColumn();
    ImGui::Text(rhs);
}

void About::content() {
    auto vspace = [](float amount = 5.0f) {
        ImGui::Dummy(ImVec2(0.0f, amount));
    };

    vspace();
    resources::logo512.draw(0.5f, true);
    vspace();
    ImGui::Text("cnd - A simple D&D 3.5 Helper");
    ImGui::Text("(c) 2024 dhir");
    ImGui::Text("https://gitlab.com/d.hir/projects/cnd");
    vspace();
    constexpr ImGuiTableFlags table_flags = ImGuiTableFlags_SizingFixedFit;
    if (ImGui::BeginTable("about_table", 3, table_flags)) {
        std::string compiler_info = std::format("{} {}", info::compiler_id, info::compiler_version);
        _row("Version:", info::version, "   ");
        _row("Compiled at:", __DATE__ ", " __TIME__);
        _row("Compiler:", compiler_info.c_str());
        _row("Git Hash:", info::git_hash_short);
        ImGui::EndTable();
    }
}


}  // namespace dhir::cnd::gui::about
