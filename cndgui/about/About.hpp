#pragma once


#include "util/Window.hpp"

namespace dhir::cnd::gui::about {


class About : public Window {
public:
    About() noexcept;

protected:
    void content() override;
};


}  // namespace dhir::cnd::gui::about
