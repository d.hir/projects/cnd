﻿// cnd.cpp : Defines the entry point for the application.
#include "cnd/character/Character.hpp"

#include <iostream>

// class Address : public jsonify::JSONable {
// public:
//     std::string& street_name = field<std::string>("Street Name", std::string("My Street"));
//     int& number = field<int>("House No.", 42);
// };

// class Person : public jsonify::JSONable {
// public:
//     Address& address = field<Address>("address");
// };

using namespace dhir::cnd;

int main() {
    // Person p;
    // // std::cout << p.address.street_name << " " << p.address.number << std::endl;
    // std::cout << p.serialize() << std::endl;

    Character c("Janette");
    std::cout << c.serialize() << std::endl;

    return 0;
}
