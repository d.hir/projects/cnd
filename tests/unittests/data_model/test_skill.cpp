#include "cnd/data_model/skill.hxx"

#include <sstream>
#include <memory>


#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(push, 0)
#endif  // _MSC_VER && !__INTEL_COMPILER

#include <catch2/catch_test_macros.hpp>

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(pop)
#endif  // _MSC_VER && !__INTEL_COMPILER


TEST_CASE("skill constructors", "[skill]") {
    SECTION("basic construct") {
        skill s{"test_skill", "strength", "test description", "phb/skill/test_skill", "1.0.0"};
        REQUIRE(s.name() == "test_skill");
        REQUIRE(s.associated_ability_score() == "strength");
        REQUIRE(s.description() == "test description");
        REQUIRE(s.id() == "phb/skill/test_skill");
        REQUIRE(s.schema_version() == "1.0.0");
    }

    SECTION("copy construct") {
        skill s{"test_skill", "strength", "test description", "phb/skill/test_skill", "1.0.0"};
        skill s2{s};
        REQUIRE(&s2 != &s);
        REQUIRE(s2.name() == "test_skill");
        REQUIRE(&s2.name() != &s.name());
        REQUIRE(s2.associated_ability_score() == "strength");
        REQUIRE(&s2.associated_ability_score() != &s.associated_ability_score());
        REQUIRE(s2.description() == "test description");
        REQUIRE(&s2.description() != &s.description());
        REQUIRE(s2.id() == "phb/skill/test_skill");
        REQUIRE(&s2.id() != &s.id());
        REQUIRE(s2.schema_version() == "1.0.0");
        REQUIRE(&s2.schema_version() != &s.schema_version());
    }
}

TEST_CASE("skill (de-)serialization", "[skill]") {
    SECTION("from xml string") {
        std::stringstream xml{"<?xml version=\"1.0\"?>"};
        xml << "<skill id=\"phb/skill/test_skill\" schema_version=\"1.0.0\">";
        xml << "  <associated_ability_score>strength</associated_ability_score>\n";
        xml << "  <name>test_skill</name>\n";
        xml << "  <description>test description</description>\n";
        xml << "</skill>\n";
        std::istringstream ixml(xml.str());
        std::unique_ptr<skill> s(skill_(xml, xml_schema::flags::dont_validate));
        REQUIRE(s->name() == "test_skill");
        REQUIRE(s->associated_ability_score() == "strength");
        REQUIRE(s->description() == "test description");
        REQUIRE(s->id() == "phb/skill/test_skill");
        REQUIRE(s->schema_version() == "1.0.0");
    }

    SECTION("to xml string") {
        skill s{"test_skill", "strength", "test description", "phb/skill/test_skill", "1.0.0"};
        std::ostringstream xml;
        skill_(xml, s);
        std::istringstream ixml(xml.str());
        std::unique_ptr<skill> s2(skill_(ixml, xml_schema::flags::dont_validate));
        REQUIRE(s2->name() == "test_skill");
        REQUIRE(s2->associated_ability_score() == "strength");
        REQUIRE(s2->description() == "test description");
        REQUIRE(s2->id() == "phb/skill/test_skill");
        REQUIRE(s2->schema_version() == "1.0.0");
    }
}