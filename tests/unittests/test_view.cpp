#include "cnd/util/view.hpp"

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(push, 0)
#endif  // _MSC_VER && !__INTEL_COMPILER

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(pop)
#endif  // _MSC_VER && !__INTEL_COMPILER


struct Nested;

struct Container {
    dhir::view<Nested> unknown;
};

struct Nested {
    unsigned long long id {42ull};
};


TEST_CASE("view constructors", "[view]") {
    SECTION("default construct") {
        dhir::view<int> v{};
        REQUIRE(v.get() == nullptr);
    }

    SECTION("construct with nullptr") {
        dhir::view<int> v(nullptr);
        REQUIRE(v.get() == nullptr);
    }

    SECTION("construct with valid pointer") {
        int value = 42;
        dhir::view<int> v(&value);
        REQUIRE(*v == 42);
    }
}

TEST_CASE("view initialization", "[view]") {
    SECTION("initialize with nullptr") {
        dhir::view<int> v;
        REQUIRE_THROWS_WITH(v.initialize(nullptr), "Cannot explicitly initialize view with nullptr (for resetting, use `view::reset`)");
    }

    SECTION("initialize with valid pointer") {
        int value = 42;
        dhir::view<int> v;
        v.initialize(&value);
        REQUIRE(*v == 42);
    }
}

TEST_CASE("view reset", "[view]") {
    int value = 42;
    dhir::view<int> v(&value);
    v.reset();
    REQUIRE(v.get() == nullptr);
}

TEST_CASE("view dereference operator", "[view]") {
    SECTION("dereference with nullptr") {
        dhir::view<int> v;
        REQUIRE_THROWS_WITH(*v, "Cannot get value of uninitialized view");
    }

    SECTION("dereference with valid pointer") {
        int value = 42;
        dhir::view<int> v(&value);
        REQUIRE(*v == 42);
    }
}

TEST_CASE("view arrow operator", "[view]") {
    SECTION("arrow with nullptr") {
        dhir::view<Nested> v;
        REQUIRE_THROWS_WITH(v->id, "Cannot get value of uninitialized view");
    }

    SECTION("arrow with valid pointer") {
        Nested value = { 137ull };
        dhir::view<Nested> v(&value);
        REQUIRE(v->id == 137ull);
    }
}

TEST_CASE("forward declared type", "[view]") {
    SECTION("default construct") {
        Container c{};
        REQUIRE(c.unknown.get() == nullptr);
    }

    SECTION("construct with valid pointer") {
        Nested value = { 137ull };
        Container c = { dhir::view<Nested>(&value) };
        REQUIRE((*c.unknown.get()).id == 137ull);
    }

    SECTION("initialize with nullptr") {
        Container c{};
        REQUIRE_THROWS_WITH(c.unknown.initialize(nullptr), "Cannot explicitly initialize view with nullptr (for resetting, use `view::reset`)");
    }

    SECTION("initialize with valid pointer") {
        Nested value = { 137ull };
        Container c{};
        c.unknown.initialize(&value);
        REQUIRE((*c.unknown).id == 137ull);
    }

    SECTION("view reset") {
        Nested value = { 137ull };
        Container c{};
        c.unknown.initialize(&value);
        c.unknown.reset();
        REQUIRE(c.unknown.get() == nullptr);
    }

    SECTION("dereference with nullptr") {
        Container c{};
        REQUIRE_THROWS_WITH(*c.unknown, "Cannot get value of uninitialized view");
    }

    SECTION("dereference with valid pointer") {
        Nested value = { 137ull };
        Container c{};
        c.unknown.initialize(&value);
        REQUIRE((*c.unknown).id == 137ull);
    }

    SECTION("arrow with nullptr") {
        Container c{};
        REQUIRE_THROWS_WITH(c.unknown->id, "Cannot get value of uninitialized view");
    }

    SECTION("arrow with valid pointer") {
        Nested value = { 137ull };
        Container c{};
        c.unknown.initialize(&value);
        REQUIRE(c.unknown->id == 137ull);
    }
}
