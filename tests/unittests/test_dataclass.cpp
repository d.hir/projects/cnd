#include "cnd/util/dataclass.hpp"

#include <string>

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(push, 0)
#endif  // _MSC_VER && !__INTEL_COMPILER

#include <catch2/catch_test_macros.hpp>

#if defined(_MSC_VER) && !defined(__INTEL_COMPILER)
#pragma warning(pop)
#endif  // _MSC_VER && !__INTEL_COMPILER


class EmptyDataclass : public dhir::dataclasses::Dataclass {};

struct SimpleDataclass : public dhir::dataclasses::Dataclass {
    int& i = field<int>("My Integer", 42);
};

struct Multifield : public dhir::dataclasses::Dataclass {
    float& f = field<float>("mafloat", 3.14f);
    unsigned long long& ull = field<unsigned long long>("maull", 137ull);
};

struct Stringfield : public dhir::dataclasses::Dataclass {
    std::string& name = field<std::string>("name", "implicitly constructed");
};

struct NestedDataclass : public dhir::dataclasses::Dataclass {
    Stringfield& props = field<Stringfield>("properties");
};

struct MultiNestedDataclass : public dhir::dataclasses::Dataclass {
    NestedDataclass& nest = field<NestedDataclass>("nested");
    Stringfield& settings = field<Stringfield>("settings");
};

bool is_whitespace(std::string::const_reference c) {
    return c == ' ' || c == '\t' || c == '\n' || c == '\r' || c == '\f' || c == '\v';
}

void strip(std::string& s) {
    s.erase(std::remove_if(s.begin(), s.end(), is_whitespace), s.end());
}

TEST_CASE("empty dataclass", "[dataclass]") {
    EmptyDataclass dc;
    SECTION("serialize") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{}");
    }
}

TEST_CASE("simple dataclass", "[dataclass]") {
    SimpleDataclass dc;
    SECTION("get default") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"MyInteger\":42}");
    }
    SECTION("after setting value") {
        dc.i = 314;
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"MyInteger\":314}");
    }
}

TEST_CASE("multifield dataclass", "[dataclass]") {
    Multifield dc;
    SECTION("get default") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"mafloat\":3.14,\"maull\":137}");
    }
    SECTION("after setting value") {
        dc.f = 2.718f;
        dc.ull = 420ull;
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"mafloat\":2.718,\"maull\":420}");
    }
}

TEST_CASE("string dataclass", "[dataclass]") {
    Stringfield dc;
    SECTION("get default") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"name\":\"implicitlyconstructed\"}");
    }
    SECTION("after setting value") {
        dc.name = "Jason";
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"name\":\"Jason\"}");
    }
}

TEST_CASE("nested dataclasses", "[dataclass]") {
    NestedDataclass dc;
    SECTION("get default") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"properties\":{\"name\":\"implicitlyconstructed\"}}");
    }
    SECTION("after setting value") {
        dc.props.name = "Jason";
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"properties\":{\"name\":\"Jason\"}}");
    }
}

TEST_CASE("multiple nested dataclasses", "[dataclass]") {
    MultiNestedDataclass dc;
    SECTION("get default") {
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(
            json == "{\"nested\":{\"properties\":{\"name\":\"implicitlyconstructed\"}},\"settings\":{\"name\":\"implicitlyconstructed\"}}"
        );
    }
    SECTION("after setting value") {
        dc.nest.props.name = "Jason";
        dc.settings.name = "Julia";
        std::string json = dc.serialize();
        strip(json);
        REQUIRE(json == "{\"nested\":{\"properties\":{\"name\":\"Jason\"}},\"settings\":{\"name\":\"Julia\"}}");
    }
}
