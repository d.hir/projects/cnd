#pragma once

#include "cnd/util/id.hpp"
#include "cnd/util/dataclass.hpp"
#include "cnd/util/libutil.hpp"

#include <optional>
#include <string>

namespace dhir::cnd {


class CND_EXPORT Character : public dataclasses::Dataclass {
private:
    id& _identifier = field<id>("ID");
    std::string& _name = field<std::string>("Name", "<unnamed>");

public:
    Character(const std::optional<std::string>& name = std::nullopt);
    Character(id&& identifier, const std::optional<std::string>& name = std::nullopt);
    Character(const id& identifier, const std::optional<std::string>& name = std::nullopt);

    Character(const Character& other) = delete;
    Character(Character&&) = default;
    Character& operator=(const Character& other) = delete;
    Character& operator=(Character&& other) = delete;
    const std::string& name() const;
};


};  // namespace dhir::cnd
