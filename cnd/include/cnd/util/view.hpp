#pragma once


#include <stdexcept>


namespace dhir {


template<typename T>
class view {
public:
    constexpr view();
    constexpr explicit view(const T * v) noexcept;

    const T * get() const noexcept;
    const T& operator*() const;
    const T * operator->() const;

    void initialize(const T * v);
    void reset();

private:
    const T * value {nullptr};
};


template<typename T>
constexpr view<T>::view() {}

template<typename T>
constexpr view<T>::view(const T * v) noexcept : value(v) {}

template<typename T>
const T * view<T>::get() const noexcept {
    return value;
}

template<typename T>
const T& view<T>::operator*() const {
    if (value == nullptr) {
        throw std::runtime_error("Cannot get value of uninitialized view");
    }
    return *value;
}

template<typename T>
const T * view<T>::operator->() const {
    if (value == nullptr) {
        throw std::runtime_error("Cannot get value of uninitialized view");
    }
    return value;
}

template<typename T>
void view<T>::initialize(const T * v) {
    if (v == nullptr) {
        throw std::runtime_error("Cannot explicitly initialize view with nullptr (for resetting, use `view::reset`)");
    }
    value = v;
}

template<typename T>
void view<T>::reset() {
    value = nullptr;
}


}  // namespace dhir
