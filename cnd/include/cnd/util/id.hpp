#pragma once

#include <algorithm>  // std::generate
#include <cstddef>    // std::size_t
#include <cstdint>    // std::uintxx_t
#include <cstring>    // std::memcpy
#include <random>
#include <vector>


#define CEIL_DIV(num, denom) ((num + denom - 1) / (denom))

namespace dhir {


namespace identifier {

template<typename T>
class Identifier;


}  // namespace identifier

using id = identifier::Identifier<std::uint64_t>;

namespace identifier {


template<typename T>
std::ostream& operator<<(std::ostream& os, const Identifier<T>& self);

template<typename T>
class Identifier {
public:
    using value_type = T;

    constexpr Identifier() noexcept = default;
    constexpr Identifier(const T& value) noexcept;

    constexpr auto operator<=>(const Identifier<T>& other) const = default;

    T * data() noexcept;
    const T * data() const noexcept;

    void randomize();
    static Identifier<T> random();

    template<typename U>
    friend std::ostream& operator<<(std::ostream& os, const Identifier<U>& self);

private:
    T value {};
};

template<typename T>
constexpr Identifier<T>::Identifier(const T& value) noexcept : value(value) {};

template<typename T>
T * Identifier<T>::data() noexcept {
    return &value;
}

template<typename T>
const T * Identifier<T>::data() const noexcept {
    return &value;
}

template<typename T>
void Identifier<T>::randomize() {
    using gen_t = std::uint32_t;
    constexpr std::size_t len = sizeof(T);
    constexpr std::size_t gen_size = sizeof(gen_t);

    std::random_device rd;
    std::uniform_int_distribution<gen_t> dist;
    std::vector<gen_t> random_data(CEIL_DIV(len, gen_size));
    std::generate(random_data.begin(), random_data.end(), [&]() { return dist(rd); });

    std::memcpy(&value, random_data.data(), len);
}

template<typename T>
Identifier<T> Identifier<T>::random() {
    identifier::Identifier<T> val;
    val.randomize();
    return val;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const Identifier<T>& self) {
    os << self.value;
    return os;
}

template std::ostream& operator<<(std::ostream& os, const Identifier<id::value_type>& self);


}  // namespace identifier


}  // namespace dhir
