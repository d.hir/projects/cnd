#pragma once

#include "cnd/util/libutil.hpp"

#include <algorithm>
#include <deque>
#include <dhir/unique_any.hpp>
#include <functional>
#include <iterator>
#include <memory>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace dhir::dataclasses {
namespace _strutil {


template<typename Range, typename Value = typename Range::value_type>
std::string join(const Range& elements, const char * const delimiter) {
    std::ostringstream os;
    auto b = begin(elements), e = end(elements);

    if (b != e) {
        std::copy(b, prev(e), std::ostream_iterator<Value>(os, delimiter));
        b = prev(e);
    }
    if (b != e) {
        os << *b;
    }

    return os.str();
}

template<typename T>
inline std::string to_string(const dhir::unique_any& value) {
    std::ostringstream os;
    os << value.get<T>();
    return os.str();
}

template<>
inline std::string to_string<std::string>(const dhir::unique_any& value) {
    return "\"" + value.get<std::string>() + "\"";
}

template<>
inline std::string to_string<bool>(const dhir::unique_any& value) {
    return value.get<bool>() ? "true" : "false";
}


}  // namespace _strutil

class Field {
public:
    using Stringify = std::function<std::string(const dhir::unique_any&)>;
    template<typename T>
    Field(const std::string& name, std::optional<Stringify> stringify, T&& object);
    Field(const Field& other) = delete;
    Field& operator=(const Field& other) = delete;

    std::string serialize(size_t indents = 0, size_t tabsize = 2) const;

    template<typename T>
    T& value();

private:
    std::string _name;
    dhir::unique_any _value;
    std::optional<Stringify> _stringify;
};

template<typename T>
Field::Field(const std::string& name, std::optional<Stringify> stringify, T&& object) :
    _name(name),
    _value(std::forward<T>(object)),
    _stringify(stringify) {
}

template<typename T>
T& Field::value() {
    return _value.get<T>();
}

class Dataclass {
public:
    CND_EXPORT std::string serialize(size_t indents = 0, size_t tabsize = 2) const;
    CND_EXPORT Dataclass() noexcept = default;
    Dataclass(const Dataclass& other) = delete;
    Dataclass& operator=(const Dataclass& other) = delete;
    Dataclass(Dataclass&& other) = default;
    Dataclass& operator=(Dataclass&& other) = default;

protected:
    template<typename T, typename... Args>
    CND_EXPORT T& field(const std::string& name, Args... constructor_args);

private:
    std::deque<Field> _fields;
};

template<typename T, typename... Args>
T& Dataclass::field(const std::string& name, Args... constructor_args) {
    Field& ref = _fields.emplace_back(name, _strutil::to_string<T>, T(constructor_args...));
    return ref.value<T>();
}

inline std::ostream& operator<<(std::ostream& os, const Dataclass& dc) {
    return os << dc.serialize();
}

};  // namespace dhir::dataclasses
