#pragma once


namespace dhir::cnd::info {


extern const char * version;
extern const char * git_hash_short;
extern const char * compiler_id;
extern const char * compiler_version;


}  // namespace dhir::cnd::info
