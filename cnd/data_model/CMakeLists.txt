add_library(data_model STATIC data_model.cpp)

set(ability_score_deps)
set(schema_version_deps)
set(skill_deps schema_version ability_score)
set(feat_deps schema_version skill)
set(character_deps schema_version skill feat ability_score)

set(xsd_bases ability_score schema_version skill feat character)

foreach(xsd_base ${xsd_bases})
    message(STATUS "Adding XSD library <${xsd_base}>")
    set(xsd_file "${CMAKE_CURRENT_SOURCE_DIR}/cnd/data_model/${xsd_base}.xsd")
    add_xsd_library(${xsd_base} ${xsd_file})
    set(deps ${xsd_base}_deps)
    message(STATUS "  deps: <${deps}> <${${deps}}>")
    if (DEFINED ${deps})
        message(STATUS "  Adding dependencies for <${xsd_base}>")
        add_dependencies(${xsd_base} ${${deps}})
    endif()
endforeach()

target_link_libraries(data_model PUBLIC ${xsd_bases})
