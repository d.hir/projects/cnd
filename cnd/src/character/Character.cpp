#include "character/Character.hpp"

namespace dhir::cnd {


Character::Character(const std::optional<std::string>& name) {
    _name = name.has_value() ? name.value() : "";
    _identifier.randomize();
}

Character::Character(id&& char_id, const std::optional<std::string>& name) {
    _name = name.has_value() ? name.value() : "";
    _identifier = char_id;
}

Character::Character(const id& char_id, const std::optional<std::string>& name) : Character::Character(id(char_id), name) {
}

const std::string& Character::name() const {
    return _name;
}


};  // namespace dhir::cnd
