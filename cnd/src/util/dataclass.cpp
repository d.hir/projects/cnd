#include "cnd/util/dataclass.hpp"

namespace dhir::dataclasses {


std::string Dataclass::serialize(size_t indents, size_t tabsize) const {
    if (_fields.empty()) {
        return "{}";
    }
    std::ostringstream os;
    std::vector<std::string> res;
    res.reserve(_fields.size());
    for (const Field& f : _fields) {
        res.emplace_back(f.serialize(indents + 1, tabsize));
    }
    os << "{\n" << _strutil::join(res, ",\n") << "\n" << std::string(indents * tabsize, ' ') << "}";
    return os.str();
}

std::string Field::serialize(size_t indents, size_t tabsize) const {
    std::ostringstream os;
    os << std::string(tabsize * indents, ' ');
    os << "\"" << _name << "\": ";
    if (_stringify.has_value()) {
        os << _stringify.value()(_value);
    } else {
        os << _value.get<Dataclass>().serialize(indents, tabsize);
    }
    return os.str();
}



}  // namespace dhir::dataclasses
