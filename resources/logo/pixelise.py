import argparse
import PIL.Image


class Image:
    def __init__(self, filename):
        im = PIL.Image.open(filename, 'r')
        with open(filename, 'rb') as f:
            self.data = [int(v) for v in f.read()]
        self.width = im.width
        self.height = im.height

    def hpp(self, varname, pix_per_line = 4):
        rgbastr = [f'0x{i:02x}' for i in self.data]
        for first_in_line in range(0, len(self.data), pix_per_line * 4):
            rgbastr[first_in_line] = '\n\t' + rgbastr[first_in_line]
        data = ', '.join(rgbastr)
        content = f'\nconst unsigned char _{varname}[] = {{'
        content += data
        content += '\n};\n\n'
        content += f'Texture {varname} {{_{varname}, {self.width}, {self.height}, {len(self.data)}}};\n\n'
        return content


def hpp(images, varnames, pix_per_line=4):
    content = '#pragma once\n\n#include "util/Texture.hpp"\n\n\nnamespace dhir::cnd::gui::resources {\n\n'
    for image, varname in zip(images, varnames):
        content += image.hpp(varname)
    content +='\n}  // namespace dhir::cnd::gui::resources\n'
    return content


def write(filename, images, varnames):
    with open(filename, 'w') as f:
        f.write(hpp(images, varnames))


def main(args):
    images = map(Image, args.images)
    write(args.hpp_file, images, args.vars)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('images', type=str, nargs='+')
    parser.add_argument('--hpp_file', type=str, default='logo.hpp')
    parser.add_argument('--vars', type=str, nargs='+')
    args = parser.parse_args()
    if args.vars is None:
        args.vars = ['.'.join(im.split('.')[:-1]) for im in args.images]
    elif len(args.vars) != len(args.images):
        raise argparse.ArgumentError(None, 'Number of vars must match number of images')
    return args


if __name__ == '__main__':
    args = parse_args()
    main(args)
